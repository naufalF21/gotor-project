package com.example.gotor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class RideMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_menu);

        ImageView kembali = findViewById(R.id.kembali);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        String page = "Ride";
        TextView pageName = findViewById(R.id.page_name);
        ImageView iconPage = findViewById(R.id.icon_page);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            page = extras.getString("pages");
        }

        switch (page) {
            case "Car" :
                pageName.setText(page);
                iconPage.setImageResource(R.drawable.gotor_car_icon_fill);
                break;
            case "Food" :
                pageName.setText(page);
                iconPage.setImageResource(R.drawable.gotor_food_icon_fill);
                break;
            case "Send" :
                pageName.setText(page);
                iconPage.setImageResource(R.drawable.gotor_send_icon_fill);
                break;
            case "Mart" :
                pageName.setText(page);
                iconPage.setImageResource(R.drawable.gotor_mart_icon_fill);
                break;
            case "Tagihan" :
                pageName.setText(page);
                iconPage.setImageResource(R.drawable.gotor_tagihan_icon_fill);
                break;
            case "Box" :
                pageName.setText(page);
                iconPage.setImageResource(R.drawable.gotor_box_icon_fill);
                break;
        }
    }
}