package com.example.gotor;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class Dashboard extends AppCompatActivity {
    public String pages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        CardView cardCari = findViewById(R.id.card_cari);
        cardCari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, Search.class));
            }
        });

        LinearLayout layoutRideMenu = findViewById(R.id.layout_ride_menu);
        layoutRideMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Dashboard.this, RideMenu.class));
            }
        });

        LinearLayout layoutCarMenu = findViewById(R.id.layout_car_menu);
        layoutCarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pages = "Car";
                Intent intent = new Intent(Dashboard.this, RideMenu.class);
                intent.putExtra("pages", pages);
                startActivity(intent);
            }
        });

        LinearLayout layoutFoodMenu = findViewById(R.id.layout_food_menu);
        layoutFoodMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pages = "Food";
                Intent intent = new Intent(Dashboard.this, RideMenu.class);
                intent.putExtra("pages", pages);
                startActivity(intent);
            }
        });

        LinearLayout layoutSendMenu = findViewById(R.id.layout_send_menu);
        layoutSendMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pages = "Send";
                Intent intent = new Intent(Dashboard.this, RideMenu.class);
                intent.putExtra("pages", pages);
                startActivity(intent);
            }
        });

        LinearLayout layoutMartMenu = findViewById(R.id.layout_mart_menu);
        layoutMartMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pages = "Mart";
                Intent intent = new Intent(Dashboard.this, RideMenu.class);
                intent.putExtra("pages", pages);
                startActivity(intent);
            }
        });

        LinearLayout layoutTagihanMenu = findViewById(R.id.layout_tagihan_menu);
        layoutTagihanMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pages = "Tagihan";
                Intent intent = new Intent(Dashboard.this, RideMenu.class);
                intent.putExtra("pages", pages);
                startActivity(intent);
            }
        });

        LinearLayout layoutBoxMenu = findViewById(R.id.layout_box_menu);
        layoutBoxMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pages = "Box";
                Intent intent = new Intent(Dashboard.this, RideMenu.class);
                intent.putExtra("pages", pages);
                startActivity(intent);
            }
        });
    }


}