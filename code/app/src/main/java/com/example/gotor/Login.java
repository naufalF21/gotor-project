package com.example.gotor;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

public class Login extends AppCompatActivity {

    Boolean inputKosong = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        MaterialButton btnLanjut = findViewById(R.id.button_lanjut);

        btnLanjut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!inputKosong) {
                    startActivity(new Intent(Login.this, Dashboard.class));
                }
            }
        });

        onClickBackButton();
        underlineText();
        onChangedButtonColor();
    }

    protected void onClickBackButton() {
        ImageView kembali = findViewById(R.id.kembali);
        kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    protected void underlineText() {
        TextView lupaPassword = findViewById(R.id.text_lupa_password);
        lupaPassword.setPaintFlags(lupaPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    }

    protected void onChangedButtonColor() {
        TextInputEditText inputEmail = findViewById(R.id.email);
        TextInputEditText inputPassword = findViewById(R.id.password);
        MaterialButton btnLanjut = findViewById(R.id.button_lanjut);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();
                int greenColor = getResources().getColor(R.color.green);
                int grayColor = getResources().getColor(R.color.gray);

                // Mengubah warna button login jika input username dan password tidak kosong
                if (!email.isEmpty() && !password.isEmpty()) {
                    btnLanjut.setBackgroundColor(greenColor);
                    inputKosong = false;
                } else {
                    btnLanjut.setBackgroundColor(grayColor);
                    inputKosong = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
        inputEmail.addTextChangedListener(textWatcher);
        inputPassword.addTextChangedListener(textWatcher);
    }
}